class_name OrbitalCamera extends Spatial

export var camera_distance: float = 8;
export var camera_height: float = 1.0;
export var mouse_sensitivity_x: float = 1.0;
export var mouse_sensitivity_y: float = 1.0;
export var joypad_sensitivity_x: float = 1.0;
export var joypad_sensitivity_y: float = 1.0;

export var camera_min_pitch_angle_degrees: float = 10;
export var camera_max_pitch_angle_degrees: float = -90;

export var camera_min_distance: float = 0.5;
export var camera_max_distance: float = 15;

onready var pitch_control_node = $PitchControlNode;
onready var camera: Camera = $PitchControlNode/Camera;

func _process(delta):

	camera.transform.origin = lerp(camera.transform.origin, Vector3.FORWARD * (-camera_distance), delta)
	pitch_control_node.transform.origin = lerp(pitch_control_node.transform.origin, Vector3.UP * camera_height, delta)

	var yaw = Input.get_action_strength("camera_yaw_right") - Input.get_action_strength("camera_yaw_left")
	var pitch = Input.get_action_strength("camera_pitch_up") - Input.get_action_strength("camera_pitch_down")

	rotate(Vector3.UP, yaw * joypad_sensitivity_x * delta);
	pitch_control_node.rotate(Vector3.RIGHT, pitch * joypad_sensitivity_y * delta)
	
	var euler: Vector3 = pitch_control_node.rotation;
	pitch_control_node.rotation = Vector3(
		clamp(euler.x, deg2rad(camera_max_pitch_angle_degrees), deg2rad(camera_min_pitch_angle_degrees)),
		euler.y,
		euler.z)

	var zoom_velocity = Input.get_action_strength("camera reverse") - Input.get_action_strength("camera_forward")
	camera_distance = clamp(camera_distance + zoom_velocity, camera_min_distance, camera_max_distance)


func _input(_event):
	pass
