shader_type spatial;
render_mode depth_draw_alpha_prepass;

uniform float cutout_size = 290.0;
uniform float cutout_distance_numerator = 5.0;
uniform float boundary_size = 30.0;
uniform vec3 visibility_target_global_position = vec3(0, 1, 0);

varying vec4 world_pos;

void vertex() {
    world_pos = WORLD_MATRIX * vec4(VERTEX, 1.0);
}

void fragment()
{
	vec4 target_in_clip_space =
		PROJECTION_MATRIX
		* INV_CAMERA_MATRIX
		* vec4(visibility_target_global_position, 1.0);
	
	vec4 target_in_camera_space = INV_CAMERA_MATRIX
		* vec4(visibility_target_global_position, 1.0);
	
	vec4 fragment_in_clip_space = 
		PROJECTION_MATRIX
		* vec4(VIEW, 1.0);
	vec3 camera_world_position = (CAMERA_MATRIX * vec4(0.0, 0.0, 0.0, 1.0)).xyz;
	
	float distance_from_fragement_to_camera = length(world_pos.xyz - camera_world_position);
	float target_dist_to_camera = length(visibility_target_global_position - camera_world_position);
	
	vec3 fragment_in_screen_space = fragment_in_clip_space.xyz / fragment_in_clip_space.w;
	fragment_in_screen_space.x *= VIEWPORT_SIZE.x;
	fragment_in_screen_space.y *= VIEWPORT_SIZE.y;
	
	vec3 target_in_screen_space = target_in_clip_space.xyz / target_in_clip_space.w;
	target_in_screen_space.x *= VIEWPORT_SIZE.x;
	target_in_screen_space.y *= VIEWPORT_SIZE.y;
	
	vec3 distance_to_target_in_screen_space = target_in_screen_space.xyz - fragment_in_screen_space.xyz;
	float distance_to_target_in_screen_space_length = length(distance_to_target_in_screen_space.xy);
	
	float distance_scale = cutout_distance_numerator / target_dist_to_camera;
	float scaled_cutout = distance_scale * cutout_size;
	
	if (distance_from_fragement_to_camera - target_dist_to_camera < 0.0) {
		
		bool evenX = (int(SCREEN_UV.x * VIEWPORT_SIZE.x / 2.0) % 2 == 0);
		bool evenY = (int(SCREEN_UV.y * VIEWPORT_SIZE.y / 2.0) % 2 == 0);
		
		if (abs(distance_to_target_in_screen_space_length) < scaled_cutout - 2.0 * boundary_size) {
			ALPHA = 0.0;
		} else if (abs(distance_to_target_in_screen_space_length) < scaled_cutout - boundary_size) {
			bool filledInX = (int(SCREEN_UV.x * VIEWPORT_SIZE.x / 6.0) % 2 == 0);
			bool filledInY = (int(SCREEN_UV.y * VIEWPORT_SIZE.y / 6.0) % 2 == 0);
			
			ALPHA = (evenX && !filledInX && !evenY && filledInY) || (!evenX && filledInX && evenY && !filledInY) ? 1.0 : 0.0;
		} else if (abs(distance_to_target_in_screen_space_length) < scaled_cutout) {
			ALPHA = (evenX && !evenY) || (!evenX && evenY) ? 1.0 : 0.0;
		}
	}
}